import xml.etree.ElementTree as ET


parser = ET.XMLParser(encoding="utf-8")
tree = ET.parse("newsafr.xml", parser)
root = tree.getroot()
channel = root[0]


def find_six_plus(list):
    list_return = []

    for i in list:
        if len(i) > 6:
            list_return.append(i.lower())

    return list_return


def top_ten(list):
    word = ''
    lst = []

    for i in range (10):
        max = 0

        for i in list:
            count = list.count(i)
            if (count > max) and (i not in lst):
                max = count
                word = i

        lst.append(word)
        print(f'{word} встречается {max} раз')


words_lst = []

for item in channel.findall('item'):
    description = item[2].text

    for i in description.split():
        words_lst.append(i)


six_plus_list = find_six_plus(words_lst)
top_ten(six_plus_list)